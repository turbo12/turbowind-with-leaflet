// Create base map 
var map = new L.Map("map", {
  center: [ 27, 5 ], // center point where map starts
  zoom: 3, // initial zoom level
  minZoom: 0, 
  maxZoom: 17});


// Distance scale in left corner
L.control.scale().addTo(map);

// Icon for turbine markers 
myIcon = L.icon({
  iconUrl: "pictures/wind_turbine1.png",
  iconSize: [40, 40],
  iconAnchor: [20,40],
  popupAnchor: [-3, -76]
});

// Clustered Markers initialization and configurations 
var markers =  L.markerClusterGroup({
  disableClusteringAtZoom: 12, 
  removeOutsideVisibleBounds: true,
  showCoverageOnHover: true,
	zoomToBoundsOnClick: true, 
  spiderfyOnMaxZoom: false,
  maxClusterRadius:80
});

// Heat layer initialization and configuration
var points = []; 
var heat = L.heatLayer(points, { 
  minOpacity: 0.6, //the minimum opacity the heat will start at
  radius: 2, //radius of each "point" of the heatmap, 25 by default
  max: 0, //maximum point intensity, 1.0 by default
  blur: 2, // amount of blur, 15 by default,            
  gradient: {
    // .4: "#4CA44C",
    .4: "#f50012",
    .6: "#d20029",
    .8: "#910034",
    1: "#4e002a"
  }
})


// load data in heat map and marker cluster 
$.get("data/data_world.csv", function(csvString) {

  // Use PapaParse to convert string to array of objects
  var data = Papa.parse(csvString, {header: true, dynamicTyping: true}).data;

  // For each coordinate in data, add to markercluster and give id 
  // add to markers frequency 0.1 and add create heat layer input
  heat_input = data.map(function(el) {

    // Also add each coordinte to Cluster Markers
    var marker = L.marker(new L.LatLng(el.lat, el.long), {icon:myIcon});
    marker.customID = el.id; // set OSM id as  custom ID for each marker
    markers.addLayer(marker);
    
    

    return [+el.lat, +el.long, 0.1];
    // return new L.LatLng(el.lat, el.long)

  });

  // give heat layer the create input 
  heat.setLatLngs(heat_input)

  // load both layers initially
  map.addLayer(markers)
  map.addLayer(heat)

  // deactivate loader
  document.getElementById("loader").classList.add("inactive");

});


// Http request on markers click to retrieve OSM information 
markers.on("click", function (e) {

  // activate loader 
  document.getElementById("loader").classList.remove("inactive");

  // call overpass api query for node id 
  var url = "https://overpass-api.de/api/interpreter?data=[out:json];nwr(" + e.layer.customID + ");out;";

  // store coordinates location
  var popLocation= e.latlng;
  var lat = popLocation["lat"];
  var long = popLocation["lng"];

  // send overpass requestx
  const Http = new XMLHttpRequest();
  Http.open("GET", url);
  Http.send();


  Http.onreadystatechange = (e) => {
    
    // parse response json file 
    var httpContent = JSON.parse(Http.responseText);
    
    // get tags information
    objTags = httpContent.elements[0].tags;

    // write popup content in html table format 
    // go through each key and value pair of tags, add to html table 
    popupContent ="<h2>Turbine Information</h2><table>";
    for(var item in objTags) {
      popupContent += ("<tr><td>"+ item +"</td><td>"+ objTags[item] +"</td></tr>");
    }
    popupContent += "</table>" +"<p>"+"Latitude, Longitude: " + lat + ", " + long + "<p>" ;

    // Add popup
    L.popup()
      .setLatLng(popLocation)
      .setContent(popupContent)
      .openOn(map); 
    
    document.getElementById("loader").classList.add("inactive");
  }

});


// Creat Legend for current wind speed overlay
// the control element is placed in the bottom right corner
var legend = L.control({position: 'bottomright'});

	legend.onAdd = function(map) {
    var div = L.DomUtil.create("div", "legend");
    div.innerHTML += '<h4>Wind speed </h4>';
    div.innerHTML += '<div class="scale">';
    div.innerHTML += '<div id="gradient-bar"></div>';
    div.innerHTML += '<div class="indicator">0 m/s</div>';
    div.innerHTML += '<div class="indicator">3 m/s</div>';
    div.innerHTML += '<div class="indicator">5 m/s</div>';
    div.innerHTML += '<div class="indicator">10 m/s</div>';
    div.innerHTML += '<div class="indicator">15 m/s</div>';
    div.innerHTML += '<div class="indicator">20 m/s</div>';
    div.innerHTML += '<div class="indicator">30 m/s</div>';
    div.innerHTML += '</div>';

    return div;
  };



// Load base layers
var Grayscale =  L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
  noWrap: true,
	maxZoom: 20
}).addTo(map);
var Satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
  noWrap: true,
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community', 
  apikey: "AAPK8f0734efe9b54824acbb11657c58cea2PGEmxAsfbQRLI6GmvjzXBOdhjun4Pnci4avo2D4A8jO2ZVJo9Aav1SywJly4NXfO"
});
var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
	maxZoom: 17,
  noWrap: true,
	attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
});
var OpenStreetMap_HOT = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
	maxZoom: 19,
  noWrap: true,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Tiles style by <a href="https://www.hotosm.org/" target="_blank">Humanitarian OpenStreetMap Team</a> hosted by <a href="https://openstreetmap.fr/" target="_blank">OpenStreetMap France</a>'
});

// Load weather map from OpenWeatherMap.org 
var OpenWeatherMap_Wind = L.tileLayer('http://maps.openweathermap.org/maps/2.0/weather/WS10/{z}/{x}/{y}?opacity=0.5&fill_bound=true&palette=0:4A9294;3:4D8E7C;5:4CA44C;10:67A436;15:A28740;20:A26D5C;30:974B91&appid=0dc692d6430e49d87909fbaf9605bcf0',{
  noWrap: true
});




// Set layers for Control 
var baseLayers = {
  "<span class='my-layer-turbine'>Grayscale</span>": Grayscale,
  "<span class='my-layer-turbine'>Satellite</span>": Satellite, 
  "<span class='my-layer-turbine'>Topographic</span> ":OpenTopoMap,
  "<span class='my-layer-turbine'>Open Street Map</span>":OpenStreetMap_HOT
};
var overlays = {
  "<span class='my-layer-turbine'>Heat Map</span>": heat,
  "<span class='my-layer-turbine'>Cluster Map</span>": markers,
  "<span class='my-layer-turbine'>Current Wind speed</span>": OpenWeatherMap_Wind
};

// Add Layer Control 
L.control.layers(baseLayers,overlays,{
  collapsed:false, 
  }).addTo(map);


// Actions when specific overlayers are added 
map.on('overlayadd', function(eo) {

  // Add Legend when current wind speed layer is added
  if (eo.name === "<span class='my-layer-turbine'>Current Wind speed</span>"){
      legend.addTo(map)
        
    var gradientBar = document.getElementById("gradient-bar");
    console.log(gradientBar)
    var barHeight = gradientBar.offsetHeight;
    var indicators = document.getElementsByClassName("indicator");
    var numberOfIndicators = indicators.length;
    var counter = 35;
    for (var x = 0; x < numberOfIndicators; x++) {
      indicators[x].style.top = counter + "px";
      counter += barHeight / numberOfIndicators + 3;
      };
  } 
});

// Actions when specific overlayers are removed 
map.on('overlayremove', function(eo) {
  // Remove Legend when current wind speed layer is added
  if (eo.name === "<span class='my-layer-turbine'>Current Wind speed</span>"){
      map.removeControl(legend);
    }
  });
  
