from os import abort
import requests


## general url
overpass_url = "http://overpass-api.de/api/interpreter?"

# ## query germany
# overpass_query = """

# [out:json];
# area["ISO3166-1"="DE"][admin_level=2];
# (node["generator:source"="wind"](area);
#  way["generator:source"="wind"](area);
#  rel["generator:source"="wind"](area);
# );
# out center;
# """
#  query world
# overpass_query = """
#     [out:json];
#     (node["generator:source"="wind"];
#     way["generator:source"="wind"];
#     rel["generator:source"="wind"];
#     );
#     out center;
#     """

overpass_query = """
    [out:json];
    (nwr["generator:source"="wind"];
    );
    out center;
    """
response = requests.get(overpass_url,
                        params={"data": overpass_query})

print(response.status_code)

datajs = response.json()


## get center from polygons  
coords = []
# i = 0
for element in datajs["elements"]:
    # power = "NA"
    # height = "NA"
    # manufacturer = "NA"
    # i = i+1
    # if i==100: 
    #     break
    
    # if "id" in element:
    #     if "generator:output:electricity" in element["tags"]:
    #         if "MW" in element["tags"]["generator:output:electricity"] or "kW" in element["tags"]["generator:output:electricity"]:
    #             power = element["tags"]["generator:output:electricity"] 
    #     if "height:hub" in element["tags"]:
    #         height = element["tags"]["height:hub"] 
    #     if "manufacturer" in element["tags"]:
    #         manufacturer = element["tags"]["manufacturer"] 
    id = element["id"]
    if element["type"] == "node":
        lon = element["lon"]
        lat = element["lat"]
        coords.append((lon, lat, id))
    elif "center" in element:
        lon = element["center"]["lon"]
        lat = element["center"]["lat"]
        coords.append((lon, lat, id))

# print(coords)

## write csv file
import csv

header = ["long", "lat", "id"]
with open('data/data_world.csv', 'w', newline='') as f:
    writer = csv.writer(f, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(header)
    writer.writerows(coords)


## create gejson file structure 
# geofile = {
#     "type": "FeatureCollection",
#     "features": [
#         {
#             "type": "Feature",
#             "geometry": {
#                 "type": "Point",
#                 "coordinates": [d[0], d[1]],
#             },
#             "properties": d,
#         } for d in coords]
# }

## write geojson file
# import json
# 
# with open("data/data_germany.json", "w") as f:
#     json.dump(geofile, f, ensure_ascii=False, indent=4)
